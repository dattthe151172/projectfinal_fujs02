-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: book_reading_management
-- ------------------------------------------------------
-- Server version	8.0.31

--
-- Database: `Book_Reading_Management`
--
CREATE DATABASE Book_Reading_Management;
USE Book_Reading_Management;
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `authority` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int not null,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  CONSTRAINT PK_user PRIMARY KEY (ID,role_id),
   FOREIGN KEY (role_id) REFERENCES role(role_id)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'1','dungnt','D30EF5569A0A3F315E0B91B0A129AE9F'),(2,'1','uyennp','77A3843713CB309A02B3C1CC2FB96E9F'),(3,'2','tuda','F93F6389D0174FE733BABC219344025E'),(4,'2','dattt','2E678024CABEBDFE17A5AEEF0163FE6D'),(5,'1','thangtn','2213046AAF694F644A5516BF1B990FC2'),(6,'2','trinhnt','00316085DB3E5C5AEFDB8E5648583C97');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_case`
--

DROP TABLE IF EXISTS `book_case`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_case` (
  `book_case_id` int NOT NULL AUTO_INCREMENT,
  `book_case_name` varchar(100) NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`book_case_id`,`user_id`),
  KEY `book_case_user_idx` (`user_id`),
  CONSTRAINT `book_case_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_case`
--

LOCK TABLES `book_case` WRITE;
/*!40000 ALTER TABLE `book_case` DISABLE KEYS */;
INSERT INTO `book_case` VALUES (1,'Barrister Bookcases',2),(2,'Modular Bookcases',4),(3,'Revolving and Corner Bookcases',6);
/*!40000 ALTER TABLE `book_case` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book` (
  `book_id` int NOT NULL AUTO_INCREMENT,
  `book_title` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `brief` varchar(255) DEFAULT NULL,
  `publisher` varchar(100) DEFAULT NULL,
  `content` text,
  `category` varchar(100) DEFAULT NULL,
  `date_of_manufacture` date DEFAULT NULL,
  `image` varchar(65535) DEFAULT null,
  `rate` int,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'A Time To Kill','John Grisham','To every thing there is a season, and a time to every purpose under the heaven','Abraham Lincoln',
'A scientific theory is a well-substantiated explanation of some aspect of the natural world, based on a body of facts that have been repeatedly confirmed through observation and experiment. Such fact-supported theories are not "guesses" but reliable accounts of the real world. The theory of biological evolution is more than "just a theory." It is as factual an explanation of the universe as the atomic theory of matter or the germ theory of disease. Our understanding of gravity is still a work in progress. But the phenomenon of gravity, like evolution, is an accepted fact',
'Novel','2022-3-3','./ASSET/images/book-1.png','2'),
(2,'Vile Bodies','Evelyn Waugh','Who shall change our vile body, that it may be fashioned like unto his glorious body, according to the working whereby he is able even to subdue all things unto himself.','Jesus Christ'
,'is a principle that has been formed as an attempt to explain things that have already been substantiated by data. It is used in the names of a number of principles accepted in the scientific community, such as the Big Bang Theory. Because of the rigors of experimentation and control, it is understood to be more likely to be true than a hypothesis is.In non-scientific use, however, hypothesis and theory are often used interchangeably to mean simply an idea, speculation, or hunch, with theory being the more common choice.',
'Novel','2022-12-9','./ASSET/images/book-2.png','3'),
(3,'Moab Is My Washpot','Stephen Fry','Moab is my washpot; over Edom will I cast out my shoe: Philistia, triumph thou because of me.','Jesus Christ',
'Since this casual use does away with the distinctions upheld by the scientific community, hypothesis and theory are prone to being wrongly interpreted even when they are encountered in scientific contexts—or at least, contexts that allude to scientific study without making the critical distinction that scientists employ when weighing hypotheses and theories.The most common occurrence is when theory is interpreted—and sometimes even gleefully seized upon—to mean something having less truth value than other scientific principles. (The word law applies to principles so firmly established that they are almost never questioned, such as the law of gravity.)',
'Novel','2022-6-5','./ASSET/images/book-3.png','4'),
(4,'Incold Blood','Truman Capote','Who cannot condemn rashness in cold blood?','Timon of Athens ',
'There are many shades of meaning to the word theory. Most of these are used without difficulty, and we understand, based on the context in which they are found, what the intended meaning is. For instance, when we speak of music theory we understand it to be in reference to the underlying principles of the composition of music, and not in reference to some speculation about those principles.
However, there are two senses of theory which are sometimes troublesome. These are the senses which are defined as “a plausible or scientifically acceptable general principle or body of principles offered to explain phenomena” and “an unproven assumption; conjecture.” The second of these is occasionally misapplied in cases where the former is meant, as when a particular scientific theory is derided as "just a theory," implying that it is no more than speculation or conjecture. One may certainly disagree with scientists regarding their theories, but it is an inaccurate interpretation of language to regard their use of the word as implying a tentative hypothesis; the scientific use of theory is quite different than the speculative use of the word.',
'Novel','2022-8-7','./ASSET/images/book-4.png','3'),
(5,'Mortal Engines','Philip Reeve','And you mortal engines whose rude throats','Matthew Arnold',
'Argentina won a ticket to the semi-finals after a controversial match against the Netherlands. Specifically, in the quarter-finals of the World Cup 2022, Spanish referee Antonio Mateu Lahoz had to draw 19 yellow cards. This is also the match with the most yellow cards in the history of the World Cup.After the Argentina - Netherlands match in the quarter-finals of the World Cup 2022, taking place at dawn on December 10 (Vietnam time), Lionel Messi is said to have severely criticized coach Louis Van Gaal and the Dutch players. Messi teammates also joined in with their captain, angering the Dutchman after winning the penalty shootout.According to information from the European press, FIFA Disciplinary Subcommittee is conducting an investigation into the controversy and scuffle between the two sides that occurred during the match. Specifically, the FIFA Disciplinary Subcommittee investigates error 12 (misbehavior against the opponent) and error 16 (violation of order and security of matches).',
'Poem','2022-3-12','./ASSET/images/book-5.png','10'),
(6,'A Time To Kill','John Grisham','To every thing there is a season, and a time to every purpose under the heaven','Abraham Lincoln',
'A scientific theory is a well-substantiated explanation of some aspect of the natural world, based on a body of facts that have been repeatedly confirmed through observation and experiment. Such fact-supported theories are not "guesses" but reliable accounts of the real world. The theory of biological evolution is more than "just a theory." It is as factual an explanation of the universe as the atomic theory of matter or the germ theory of disease. Our understanding of gravity is still a work in progress. But the phenomenon of gravity, like evolution, is an accepted fact',
'Novel','2022-3-3','./ASSET/images/book-1.png','11'),
(7,'Vile Bodies','Evelyn Waugh','Who shall change our vile body, that it may be fashioned like unto his glorious body, according to the working whereby he is able even to subdue all things unto himself.','Jesus Christ'
,'is a principle that has been formed as an attempt to explain things that have already been substantiated by data. It is used in the names of a number of principles accepted in the scientific community, such as the Big Bang Theory. Because of the rigors of experimentation and control, it is understood to be more likely to be true than a hypothesis is.In non-scientific use, however, hypothesis and theory are often used interchangeably to mean simply an idea, speculation, or hunch, with theory being the more common choice.',
'Novel','2022-12-9','./ASSET/images/book-2.png','20'),
(8,'Moab Is My Washpot','Stephen Fry','Moab is my washpot; over Edom will I cast out my shoe: Philistia, triumph thou because of me.','Jesus Christ',
'Since this casual use does away with the distinctions upheld by the scientific community, hypothesis and theory are prone to being wrongly interpreted even when they are encountered in scientific contexts—or at least, contexts that allude to scientific study without making the critical distinction that scientists employ when weighing hypotheses and theories.The most common occurrence is when theory is interpreted—and sometimes even gleefully seized upon—to mean something having less truth value than other scientific principles. (The word law applies to principles so firmly established that they are almost never questioned, such as the law of gravity.)',
'Novel','2022-6-5','./ASSET/images/book-3.png','50'),
(9,'Incold Blood','Truman Capote','Who cannot condemn rashness in cold blood?','Timon of Athens ',
'There are many shades of meaning to the word theory. Most of these are used without difficulty, and we understand, based on the context in which they are found, what the intended meaning is. For instance, when we speak of music theory we understand it to be in reference to the underlying principles of the composition of music, and not in reference to some speculation about those principles.
However, there are two senses of theory which are sometimes troublesome. These are the senses which are defined as “a plausible or scientifically acceptable general principle or body of principles offered to explain phenomena” and “an unproven assumption; conjecture.” The second of these is occasionally misapplied in cases where the former is meant, as when a particular scientific theory is derided as "just a theory," implying that it is no more than speculation or conjecture. One may certainly disagree with scientists regarding their theories, but it is an inaccurate interpretation of language to regard their use of the word as implying a tentative hypothesis; the scientific use of theory is quite different than the speculative use of the word.',
'Novel','2022-8-7','./ASSET/images/book-4.png','3'),
(10,'Mortal Engines','Philip Reeve','And you mortal engines whose rude throats','Matthew Arnold',
'Argentina won a ticket to the semi-finals after a controversial match against the Netherlands. Specifically, in the quarter-finals of the World Cup 2022, Spanish referee Antonio Mateu Lahoz had to draw 19 yellow cards. This is also the match with the most yellow cards in the history of the World Cup.After the Argentina - Netherlands match in the quarter-finals of the World Cup 2022, taking place at dawn on December 10 (Vietnam time), Lionel Messi is said to have severely criticized coach Louis Van Gaal and the Dutch players. Messi teammates also joined in with their captain, angering the Dutchman after winning the penalty shootout.According to information from the European press, FIFA Disciplinary Subcommittee is conducting an investigation into the controversy and scuffle between the two sides that occurred during the match. Specifically, the FIFA Disciplinary Subcommittee investigates error 12 (misbehavior against the opponent) and error 16 (violation of order and security of matches).',
'Poem','2022-3-12','./ASSET/images/book-5.png','30'),
(11,'A Time To Kill','John Grisham','To every thing there is a season, and a time to every purpose under the heaven','Abraham Lincoln',
'A scientific theory is a well-substantiated explanation of some aspect of the natural world, based on a body of facts that have been repeatedly confirmed through observation and experiment. Such fact-supported theories are not "guesses" but reliable accounts of the real world. The theory of biological evolution is more than "just a theory." It is as factual an explanation of the universe as the atomic theory of matter or the germ theory of disease. Our understanding of gravity is still a work in progress. But the phenomenon of gravity, like evolution, is an accepted fact',
'Novel','2022-3-3','./ASSET/images/book-1.png','40'),
(12,'Vile Bodies','Evelyn Waugh','Who shall change our vile body, that it may be fashioned like unto his glorious body, according to the working whereby he is able even to subdue all things unto himself.','Jesus Christ'
,'is a principle that has been formed as an attempt to explain things that have already been substantiated by data. It is used in the names of a number of principles accepted in the scientific community, such as the Big Bang Theory. Because of the rigors of experimentation and control, it is understood to be more likely to be true than a hypothesis is.In non-scientific use, however, hypothesis and theory are often used interchangeably to mean simply an idea, speculation, or hunch, with theory being the more common choice.',
'Novel','2022-12-9','./ASSET/images/book-2.png','31'),
(13,'Moab Is My Washpot','Stephen Fry','Moab is my washpot; over Edom will I cast out my shoe: Philistia, triumph thou because of me.','Jesus Christ',
'Since this casual use does away with the distinctions upheld by the scientific community, hypothesis and theory are prone to being wrongly interpreted even when they are encountered in scientific contexts—or at least, contexts that allude to scientific study without making the critical distinction that scientists employ when weighing hypotheses and theories.The most common occurrence is when theory is interpreted—and sometimes even gleefully seized upon—to mean something having less truth value than other scientific principles. (The word law applies to principles so firmly established that they are almost never questioned, such as the law of gravity.)',
'Novel','2022-6-5','./ASSET/images/book-3.png','4'),
(14,'Incold Blood','Truman Capote','Who cannot condemn rashness in cold blood?','Timon of Athens ',
'There are many shades of meaning to the word theory. Most of these are used without difficulty, and we understand, based on the context in which they are found, what the intended meaning is. For instance, when we speak of music theory we understand it to be in reference to the underlying principles of the composition of music, and not in reference to some speculation about those principles.
However, there are two senses of theory which are sometimes troublesome. These are the senses which are defined as “a plausible or scientifically acceptable general principle or body of principles offered to explain phenomena” and “an unproven assumption; conjecture.” The second of these is occasionally misapplied in cases where the former is meant, as when a particular scientific theory is derided as "just a theory," implying that it is no more than speculation or conjecture. One may certainly disagree with scientists regarding their theories, but it is an inaccurate interpretation of language to regard their use of the word as implying a tentative hypothesis; the scientific use of theory is quite different than the speculative use of the word.',
'Novel','2022-8-7','./ASSET/images/book-4.png','35'),
(15,'Mortal Engines','Philip Reeve','And you mortal engines whose rude throats','Matthew Arnold',
'Argentina won a ticket to the semi-finals after a controversial match against the Netherlands. Specifically, in the quarter-finals of the World Cup 2022, Spanish referee Antonio Mateu Lahoz had to draw 19 yellow cards. This is also the match with the most yellow cards in the history of the World Cup.After the Argentina - Netherlands match in the quarter-finals of the World Cup 2022, taking place at dawn on December 10 (Vietnam time), Lionel Messi is said to have severely criticized coach Louis Van Gaal and the Dutch players. Messi teammates also joined in with their captain, angering the Dutchman after winning the penalty shootout.According to information from the European press, FIFA Disciplinary Subcommittee is conducting an investigation into the controversy and scuffle between the two sides that occurred during the match. Specifically, the FIFA Disciplinary Subcommittee investigates error 12 (misbehavior against the opponent) and error 16 (violation of order and security of matches).',
'Poem','2022-3-12','./ASSET/images/book-5.png','49');

/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contain`
--

DROP TABLE IF EXISTS `contain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contain` (
  `book_case_id` int NOT NULL,
  `book_id` int NOT NULL,
  `create_date` date DEFAULT NULL,
  PRIMARY KEY (`book_case_id`,`book_id`),
  KEY `book_id_idx` (`book_id`),
  CONSTRAINT `book_case_id` FOREIGN KEY (`book_case_id`) REFERENCES `book_case` (`book_case_id`),
  CONSTRAINT `book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contain`
--

LOCK TABLES `contain` WRITE;
/*!40000 ALTER TABLE `contain` DISABLE KEYS */;
INSERT INTO `contain` VALUES (1,2,'1997-06-12'),(2,3,'1987-06-04'),(1,4,'1999-09-14'),(3,5,'1977-12-18'),(4,1,'1993-04-12');
/*!40000 ALTER TABLE `contain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--





/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-05 11:47:24
