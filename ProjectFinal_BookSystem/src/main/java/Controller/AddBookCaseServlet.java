package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

import DAO.BookCaseDao;
import DAO.Impl.BookCaseDaoImpl;
import Model.User;

/**
 * Servlet implementation class AddBookCaseServlet
 */
public class AddBookCaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddBookCaseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int bookId = Integer.parseInt(request.getParameter("bookId")) ;
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		BookCaseDao bookCaseDao = new BookCaseDaoImpl();
		int bookcase =  bookCaseDao.getBookCaseByUserId(user.getId()).getBookCaseId();		
		bookCaseDao.addBookCase(bookId, bookcase);
		response.sendRedirect("bookcase");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
