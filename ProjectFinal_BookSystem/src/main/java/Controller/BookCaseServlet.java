package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import DAO.bookIFDao;
import DAO.Impl.BookCaseDaoImpl;
import DAO.Impl.bookDao;
import Model.Book;
import Model.User;

/**
 * Servlet implementation class BookCaseServlet
 */
public class BookCaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookCaseServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		BookCaseDaoImpl bookCaseDaoImpl = new BookCaseDaoImpl();
		String indexPage = request.getParameter("index");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		if (user == null) {
			response.sendRedirect("login");
		} else {
			int id = user.getId();
			if (indexPage == null) {
				indexPage = "1";
			}
			int index = Integer.parseInt(indexPage);
			int count = bookCaseDaoImpl.getTotalBookCaseByUserId(id);
			int endPage = count / 9;
			if (count % 9 != 0) {
				endPage++;
			}

			ArrayList<Book> books = bookCaseDaoImpl.getBookByUserId(id, index);
			request.setAttribute("index", index);
			request.setAttribute("endP", endPage);
			request.setAttribute("books", books);
			request.setAttribute("id", id);
			request.getRequestDispatcher("./JSP/view_bookcase.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
