package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import DAO.bookIFDao;
import DAO.Impl.bookDao;
import Model.Book;

/**
 * Servlet implementation class BookDetailServlet
 */
public class BookDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		bookIFDao daoBook = new bookDao();
		int bookId = Integer.parseInt(request.getParameter("bookId"));
//		int bookId = 1;
		List<Book> listDateDESC = daoBook.getListBookBydateOfManufDESC();
		Book book = daoBook.getBookByBookId(bookId);

		request.setAttribute("book", book);
		request.setAttribute("listDateDESC", listDateDESC);
		request.getRequestDispatcher("./JSP/book_detail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
