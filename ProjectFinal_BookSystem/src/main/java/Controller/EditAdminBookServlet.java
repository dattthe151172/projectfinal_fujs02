package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import DAO.Impl.bookDao;
import Model.Book;

/**
 * Servlet implementation class EditAdminBookServlet
 */
public class EditAdminBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditAdminBookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		bookDao dao = new bookDao();
		if(request.getParameter("id")==null) {		
			response.sendRedirect("admin");
		}else {
			 int id = Integer.parseInt(request.getParameter("id"));
			Book book = dao.getBookByBookId(id);
			request.setAttribute("book", book);
			request.getRequestDispatcher("./JSP/admin_editbook.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		bookDao dao = new bookDao();
		if(request.getParameter("id")==null) {		
			response.sendRedirect("admin");
		}else {
			int id = Integer.parseInt(request.getParameter("id"));
			Book book = dao.getBookByBookId(id);
			String name = request.getParameter("name");
			String author = request.getParameter("author");
			String category = request.getParameter("category");
			String Brief = request.getParameter("Brief");
			String Publisher = request.getParameter("Publisher");
		    String Content = request.getParameter("Content");
		    Book books = new Book(id, name,author,Brief,Publisher,Content,category,book.getDateOfManuf(),book.getImage(),book.getRate());
			dao.EditAdminBook(books);
			response.sendRedirect("admin");
		}
	}
}
