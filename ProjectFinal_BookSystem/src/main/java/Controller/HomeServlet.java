package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import DAO.bookIFDao;
import DAO.Impl.bookDao;
import Model.Book;

/**
 * Servlet implementation class HomeServlet
 */
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		bookIFDao daoBook = new bookDao();

		List<Book> listDateDESC = daoBook.getListBookBydateOfManufDESC();
		List<Book> listRateDESC = daoBook.getListBookByRateDESC();
		List<Book> listBook = daoBook.getListAllBook();

		request.setAttribute("listBook", listBook);
		request.setAttribute("listRateDESC", listRateDESC);
		request.setAttribute("listDateDESC", listDateDESC);
		request.getRequestDispatcher("./JSP/home.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
