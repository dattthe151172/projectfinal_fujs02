package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import DAO.bookIFDao;
import DAO.Impl.bookDao;
import Model.Book;
import Model.User;

/**
 * Servlet implementation class ListBookAdminServlet
 */
public class ListBookAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListBookAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		bookIFDao daoBook = new bookDao();
		String indexPage = request.getParameter("index");
		if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        int count = daoBook.getTotalBook();
        int endPage = count / 9;
        if (count % 9 != 0) {
            endPage++;
        } 
		List<Book> listAllBook = daoBook.getListAllBook(index);
		request.setAttribute("index", index);
		request.setAttribute("endP", endPage);
		request.setAttribute("listAllBook", listAllBook);
		request.getRequestDispatcher("./JSP/admin_listbook.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
