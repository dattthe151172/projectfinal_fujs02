package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


import java.io.IOException;

import DAO.MD5IFDao;
import DAO.userDao;
import DAO.Impl.MD5Dao;
import Model.User;



/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("JSP/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		userDao dao = new userDao();
		HttpSession session = request.getSession();
		MD5IFDao MD5dao = new MD5Dao();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String passwordMD5 = MD5dao.CheckPasswordMD5(password);
		try {
			User user = dao.getUser(username, passwordMD5);
			if(user != null & user.getRoleId()==1) {
				int id = user.getId();
				session.setAttribute("user", user);
				session.setAttribute("id", id);
				
				response.sendRedirect("admin");
			}else if(user != null & user.getRoleId()==2){
				session.setAttribute("user", user);
				response.sendRedirect("Home");
			}

		} catch (Exception e) {
			request.setAttribute("message", "Tên đăng nhập hoặc mật khẩu không đúng!!!");
			request.getRequestDispatcher("JSP/login.jsp").forward(request, response);
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
