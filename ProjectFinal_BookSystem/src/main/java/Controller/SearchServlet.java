package Controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import DAO.bookIFDao;
import DAO.Impl.bookDao;
import Model.Book;

/**
 * Servlet implementation class SearchServlet
 */
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		bookDao dao = new bookDao();		
		String indexPage = request.getParameter("index");
		if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        int total = 0;
		String text = request.getParameter("search");
        ArrayList<Book> books = dao.searchBook(text, index);
		for (Book book : books) {
			total++;
		}
        int endPage = total / 9;
        if (total % 9 != 0) {
            endPage++;
        }
		
		request.setAttribute("search", text);
		request.setAttribute("index", index);
		request.setAttribute("endP", endPage);
		request.setAttribute("total", total);
		request.setAttribute("books", books);
		request.getRequestDispatcher("./JSP/search_book.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
