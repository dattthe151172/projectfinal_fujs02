package DAO;

import java.util.ArrayList;

import Model.Book;
import Model.BookCase;

public interface BookCaseDao {
	ArrayList<Book> getBookByUserId(int id, int index);
	void addBookCase(int bookid, int bookcaseid);
	BookCase getBookCaseByUserId(int id);
	void deleteBookCaseById(int bookCaseId,int bookId);
	int getTotalBookCaseByUserId(int userId);
}
