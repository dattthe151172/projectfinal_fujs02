package DAO.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import DAO.BookCaseDao;
import DB.DBContext;
import Model.Book;
import Model.BookCase;

public class BookCaseDaoImpl extends DBContext implements BookCaseDao {
	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement preSta = null;
	
	@Override
	public ArrayList<Book> getBookByUserId(int id, int index) {
		ArrayList<Book> books = new ArrayList<>();
		String sql = "SELECT b.book_id, b.book_title, b.author, b.category, b.publisher, b.image \r\n"
				+ "FROM book_reading_management.contain bc \r\n"
				+ "left join book_reading_management.book b \r\n"
				+ "on b.book_id = bc.book_id \r\n"
				+ "right join book_reading_management.book_case bcc \r\n"
				+ "on bc.book_case_id = bcc.book_case_id\r\n"
				+ "where bcc.user_id = ?\r\n"
				+ "ORDER BY b.book_id limit ?, 9;";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(1, id);
			preSta.setInt(2, (index - 1) * 9);
			rs = preSta.executeQuery();
			while (rs.next()) {
				Book book = new Book();
				book.setBookId(rs.getInt("book_id"));
				book.setBookTitle(rs.getString("book_title"));
				book.setAuthor(rs.getString("author"));
				book.setCategory(rs.getString("category"));
				book.setPublisher(rs.getString("publisher"));
				book.setImage(rs.getString("image"));
				books.add(book);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		return books;
	}
	@Override
	public void addBookCase(int bookid, int bookcaseid) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO `book_reading_management`.`contain` (`book_case_id`, `book_id`) VALUES (?, ?);";
		try {
		conn = getConnection();
		preSta = conn.prepareStatement(sql);
		preSta.setInt(2, bookid);
		preSta.setInt(1, bookcaseid);
		preSta.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	@Override
	public BookCase getBookCaseByUserId(int id) {
		BookCase bookCase = null;
		String sql = "SELECT * FROM book_reading_management.book_case where user_id = ?";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(1, id);
			rs = preSta.executeQuery();
			if(rs.next()) {
				bookCase = new BookCase();
				bookCase.setBookCaseId(rs.getInt("book_case_id"));
				bookCase.setBookCaseName(rs.getString("book_case_name"));
				bookCase.setUserId(rs.getInt("user_id"));
			}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		return bookCase;
	}
	@Override
	public void deleteBookCaseById(int bookCaseId, int bookId) {
		String sql = "DELETE FROM `book_reading_management`.`contain` WHERE (`book_case_id` = ?) and (`book_id` = ?);\r\n"
				+ "";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(2, bookId);
			preSta.setInt(1, bookCaseId);
			preSta.executeUpdate();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		
	}
	
	public static void main(String[] args) {
		BookCaseDaoImpl dao = new BookCaseDaoImpl();
		ArrayList<Book> books = dao.getBookByUserId(2, 1);
		for (Book book : books) {
			System.out.println(book);
			System.err.println("hi");

		}
	}
	@Override
	public int getTotalBookCaseByUserId(int userId) {
		String sql = "SELECT count(*) FROM book_reading_management.contain c\r\n"
				+ "inner join book_reading_management.book_case bc \r\n"
				+ "on c.book_case_id = bc.book_case_id\r\n"
				+ "where bc.user_id = ?;";
        try {
        	conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(1, userId);
			rs = preSta.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
		return 0;
	}

}
