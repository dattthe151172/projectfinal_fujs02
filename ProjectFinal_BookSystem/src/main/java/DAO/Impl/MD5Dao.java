package DAO.Impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import DAO.MD5IFDao;
import DB.DBContext;
import jakarta.xml.bind.DatatypeConverter;

public class MD5Dao implements MD5IFDao {
	
	 public String convertHashToString(String text) throws NoSuchAlgorithmException {
//       MessageDigest md = MessageDigest.getInstance("MD5");
//       byte[] hashInBytes = md.digest(text.getBytes(StandardCharsets.UTF_8));
//       StringBuilder sb = new StringBuilder();
//       for (byte b : hashInBytes) {
//           sb.append(String.format("%02x", b));
//       }
       MessageDigest md = MessageDigest.getInstance("MD5");
       md.update(text.getBytes());
       byte[] digest = md.digest();
       String myHash = DatatypeConverter
               .printHexBinary(digest).toUpperCase();
//       return sb.toString();
       return myHash;
   }

	 @Override
   public String CheckPasswordMD5(String pass) {
       MD5Dao md5 = new MD5Dao();
       
       String passInData = null;
       try {
           passInData = md5.convertHashToString(pass);
       } catch (NoSuchAlgorithmException ex) {
           Logger.getLogger(MD5Dao.class.getName()).log(Level.SEVERE, null, ex);
       }

       return passInData;
   }

   public static boolean verify(String inputPassword, String hashPassWord)
           throws NoSuchAlgorithmException {
       MessageDigest md = MessageDigest.getInstance("MD5");
       md.update(inputPassword.getBytes());
       byte[] digest = md.digest();
       String myChecksum = DatatypeConverter
               .printHexBinary(digest).toUpperCase();
       return hashPassWord.equals(myChecksum);
   }

   public static void main(String[] args) throws NoSuchAlgorithmException {
       MD5Dao md5 = new MD5Dao();
//       System.out.println(md5.convertHashToString("123456"));
//       String pass = md5.CheckPasswordMD5("nam123");
       String pass = md5.convertHashToString("thang123");
       System.out.println(pass);
   }
}
