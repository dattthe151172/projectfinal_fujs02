package DAO.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import DAO.bookIFDao;
import DB.DBContext;
import Model.Book;

public class bookDao extends DBContext implements bookIFDao {

	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement preSta = null;

	@Override
	public List<Book> getListBookBydateOfManufDESC() {
		List<Book> list = new ArrayList<>();
		String sql = "SELECT  * FROM   `book`\r\n" + "ORDER BY  `date_of_manufacture` DESC";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			rs = preSta.executeQuery();
			while (rs.next()) {
				list.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getInt(10)));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		return list;
	}
	
	

	@Override
	public List<Book> getListBookByRateDESC() {
		List<Book> list = new ArrayList<>();
		String sql = "SELECT  * FROM   `book`\r\n" + "ORDER BY  `rate` DESC";
		int count = 0;
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			rs = preSta.executeQuery();
			while (rs.next()) {
				if (count < 6) {
					list.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
							rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getInt(10)));
					count++;
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		return list;
	}

	@Override
	public ArrayList<Book> searchBook(String text, int index) {
		ArrayList<Book> books = new ArrayList<>();
		String sql = "SELECT *\r\n"
				+ "FROM book_reading_management.book\r\n"
				+ "WHERE INSTR(`book_title`, ?) > 0\r\n"
				+ "or INSTR(`author`, ?) > 0\r\n"
				+ "or category = ? \n"
				+ "ORDER BY book_reading_management.book.book_id limit ?, 9;";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setString(1, text);
			preSta.setString(2, text);
			preSta.setString(3, text);
			preSta.setInt(4, (index - 1) * 9);
			rs = preSta.executeQuery();
			while (rs.next()) {
				Book book = new Book();
				book.setBookId(rs.getInt("book_id"));
				book.setBookTitle(rs.getString("book_title"));
				book.setAuthor(rs.getString("author"));
				book.setBrief(rs.getString("brief"));
				book.setPublisher(rs.getString("publisher"));
				book.setContent(rs.getString("content"));
				book.setCategory(rs.getString("category"));
				book.setDateOfManuf(rs.getString("date_of_manufacture"));
				book.setImage(rs.getString("image"));
				book.setRate(rs.getInt("rate"));
				books.add(book);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		return books;
	}
	@Override
	public List<Book> getListAllBook() {
		List<Book> list = new ArrayList<>();
		String sql = "SELECT * FROM book_reading_management.book";
		
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			
			rs = preSta.executeQuery();
			while (rs.next()) {

				list.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getInt(10)));

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		return list;
	}
	
	@Override
	public List<Book> getListAllBook(int index) {
		List<Book> list = new ArrayList<>();
		String sql = "SELECT * FROM book_reading_management.book"
				+ " ORDER BY book_reading_management.book.book_id limit ?, 9;";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(1, (index - 1) * 9);
			rs = preSta.executeQuery();
			while (rs.next()) {

				list.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getInt(10)));

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		return list;
	}
	
	@Override
	public int getTotalBook() {
        String sql = "SELECT count(*) FROM book_reading_management.book ;";
        try {
        	conn = getConnection();
			preSta = conn.prepareStatement(sql);
			rs = preSta.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }
	
	@Override
	public Book getBookByBookId(int bookId) {
		Book book = new Book();
		String sql = "SELECT  * FROM   book_reading_management.book "
				+ "where `book`.`book_id` = ?;";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(1, bookId);
			rs = preSta.executeQuery();
			while (rs.next()) {
				book.setBookId(rs.getInt("book_id"));
				book.setBookTitle(rs.getString("book_title"));
				book.setAuthor(rs.getString("author"));
				book.setBrief(rs.getString("brief"));
				book.setPublisher(rs.getString("publisher"));
				book.setContent(rs.getString("content"));
				book.setCategory(rs.getString("category"));
				book.setDateOfManuf(rs.getString("date_of_manufacture"));
				book.setImage(rs.getString("image"));
				book.setRate(rs.getInt("rate"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		return book;
	}



	@Override
	public void addBook(Book book) {
		try {
            String sql = "INSERT INTO `book_reading_management`.`book`\r\n"
            		+ "(`book_title`,\r\n"
            		+ "`author`,\r\n"
            		+ "`brief`,\r\n"
            		+ "`publisher`,\r\n"
            		+ "`content`,\r\n"
            		+ "`category`,\r\n"
            		+ "`date_of_manufacture`,\r\n"
            		+ "`image`)\r\n"
            		+ "VALUES\r\n"
            		+ "(?,\r\n"
            		+ "?,\r\n"
            		+ "?,\r\n"
            		+ "?,\r\n"
            		+ "?,\r\n"
            		+ "?,\r\n"
            		+ "?,\r\n"
            		+ "?);\r\n"
            		+ "";
            conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setString(1, book.getBookTitle());
			preSta.setString(2, book.getAuthor());
			preSta.setString(3, book.getBrief());
			preSta.setString(4, book.getPublisher());
			preSta.setString(5, book.getContent());
			preSta.setString(6, book.getCategory());
			preSta.setString(7, book.getDateOfManuf());
			preSta.setString(8, book.getImage());
			preSta.executeUpdate();

        } catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(conn, preSta, rs);
		}
		
	}

	@Override
	public void deleteAdminBook(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM `book_reading_management`.`book` WHERE (`book_id` = ?);";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(1, id);
			preSta.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			closeConnection(conn, preSta, rs);
		}
	}
	
	public void deleteAdminBookContain(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM `book_reading_management`.`contain` WHERE (`book_id` = ?);";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setInt(1, id);
			preSta.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			closeConnection(conn, preSta, rs);
		}
	}

@Override
public void EditAdminBook(Book book) {
	// TODO Auto-generated method stub
	String sql = "UPDATE `book`\r\n"
			+ "SET\r\n"
			+ "`book_title` = ?,\r\n"
			+ "`author` = ?,\r\n"
			+ "`brief` = ?,\r\n"
			+ "`publisher` = ?,\r\n"
			+ "`content` = ?,\r\n"
			+ "`category` = ?\r\n"
			+ "WHERE `book_id` = ?;";

	try {
		conn = getConnection();	
		preSta = conn.prepareStatement(sql);	
		preSta.setString(1, book.getBookTitle());
		preSta.setString(2, book.getAuthor());
		preSta.setString(3, book.getBrief());
		preSta.setString(4, book.getPublisher());
		preSta.setString(5, book.getContent());
		preSta.setString(6, book.getCategory());
		preSta.setInt(7, book.getBookId());
		preSta.executeUpdate();
		
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	
}
		public static void main(String[] args) {
			bookDao dao = new bookDao();
			dao.deleteAdminBook(1);
		}
		
	}

