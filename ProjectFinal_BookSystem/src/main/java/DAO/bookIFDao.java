package DAO;


import java.util.ArrayList;
import java.util.List;

import Model.Book;

public interface bookIFDao {

	List<Book> getListBookBydateOfManufDESC() ;
	ArrayList<Book> searchBook(String text, int index);
	List<Book> getListBookByRateDESC();
	List<Book> getListAllBook(int index);
	int getTotalBook();
	Book getBookByBookId(int bookId);
	List<Book> getListAllBook();
	void addBook(Book book);
	void deleteAdminBook(int id);
	void EditAdminBook(Book book);
}
