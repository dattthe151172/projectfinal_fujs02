package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DB.DBContext;
import Model.User;


public class userDao extends DBContext {
	Connection conn = null;
	PreparedStatement preSta = null;
	ResultSet rs = null;
	
	public User getUser(String username, String password) throws SQLException {
		// TODO Auto-generated method stub
		User user = null;
		String sql= "Select *from user where user_name = ? and password = ?";
		try {
			conn = getConnection();
			preSta = conn.prepareStatement(sql);
			preSta.setString(1, username);
			preSta.setString(2, password);
			rs = preSta.executeQuery();
			while(rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("user_name"));
				user.setPassword(rs.getString("password"));
				user.setRoleId(rs.getInt("role_id"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			closeConnection(conn, preSta, rs);
		}
		return user;
	}
	
	public static void main(String[] args) {
		userDao dao= new userDao();
		try {
			User user = dao.getUser("dungnt", "dung123");
			System.out.println(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

	
