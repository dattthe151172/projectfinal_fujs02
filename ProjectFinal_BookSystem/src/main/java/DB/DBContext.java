package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBContext {
//	public static Connection getConnection() {
//		String connectionUrl = "jdbc:mysql://localhost:3306/ASG3";
//		Connection conn = null;
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//			conn = DriverManager.getConnection(connectionUrl, "root", "123dat69");
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return conn;
//	}
	
	
	public Connection getConnection()throws Exception {
        String url = "jdbc:mysql://"+serverName+":"+portNumber +"/"+dbName+"?allowPublicKeyRetrieval=true&useSSL=false";
        Class.forName("com.mysql.cj.jdbc.Driver");
        return DriverManager.getConnection(url, userID, password);
    }

    /*Insert your other code right after this comment*/

    /*Change/update information of your database connection, DO NOT change name of instance variables in this class*/
    private final String serverName = "127.0.0.1";
    private final String dbName = "book_reading_management";
    private final String portNumber = "3306";
    private final String userID = "root";
    private final String password = "123dat69";
    
     

    
    public static void main(String[] args) {
        try{
            DBContext db = new DBContext() ;
            if (db.getConnection() != null)
            {
                    System.out.println("ket noi thanh cong");
            }else
            {
                System.out.println("Ket noi that bai");
            }

        }catch(Exception e ) {
            System.out.println(e);
        }
    }
	
	public static void closeConnection(Connection conn, Statement stmt, ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
			if(stmt != null) {
				stmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void closeConnection(Connection conn, PreparedStatement stmt, ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
			if(stmt != null) {
				stmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
