package Model;

public class Book {
	private int bookId;
	private String bookTitle;
	private String author;
	private String brief;
	private String publisher;
	private String content;
	private String category;
	private String dateOfManuf;
	private String image;
	private int rate;
	private BookCase bookCase;
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book(int bookId, String bookTitle, String author, String brief, String publisher, String content,
			String category, String dateOfManuf, String image, int rate) {
		super();
		this.bookId = bookId;
		this.bookTitle = bookTitle;
		this.author = author;
		this.brief = brief;
		this.publisher = publisher;
		this.content = content;
		this.category = category;
		this.dateOfManuf = dateOfManuf;
		this.image = image;
		this.rate = rate;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDateOfManuf() {
		return dateOfManuf;
	}
	public void setDateOfManuf(String dateOfManuf) {
		this.dateOfManuf = dateOfManuf;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookTitle=" + bookTitle + ", author=" + author + ", brief=" + brief
				+ ", publisher=" + publisher + ", content=" + content + ", category=" + category + ", dateOfManuf="
				+ dateOfManuf + ", image=" + image + ", rate=" + rate + "]";
	}
	public BookCase getBookCase() {
		return bookCase;
	}
	public void setBookCase(BookCase bookCase) {
		this.bookCase = bookCase;
	}
	
	
	
	
}
