package Model;

public class BookCase {
	private int bookCaseId;
	private String bookCaseName;
	private int userId;
	public BookCase() {
		// TODO Auto-generated constructor stub
	}
	public int getBookCaseId() {
		return bookCaseId;
	}
	public void setBookCaseId(int bookCaseId) {
		this.bookCaseId = bookCaseId;
	}
	public String getBookCaseName() {
		return bookCaseName;
	}
	public void setBookCaseName(String bookCaseName) {
		this.bookCaseName = bookCaseName;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
}
