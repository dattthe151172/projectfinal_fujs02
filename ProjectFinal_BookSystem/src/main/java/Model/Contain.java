package Model;

import java.util.ArrayList;

public class Contain {
	private int bookCaseId;
	private int bookId;
	private String createdDay;
	private ArrayList<Book> books = new ArrayList<>();
	
	
	
	public ArrayList<Book> getBooks() {
		return books;
	}

	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}

	public Contain() {
		// TODO Auto-generated constructor stub
	}

	public int getBookCaseId() {
		return bookCaseId;
	}

	public void setBookCaseId(int bookCaseId) {
		this.bookCaseId = bookCaseId;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getCreatedDay() {
		return createdDay;
	}

	public void setCreatedDay(String createdDay) {
		this.createdDay = createdDay;
	}
	
	
	
	
}
