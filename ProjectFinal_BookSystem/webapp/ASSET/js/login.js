function Validator() {
    var UserNameElement = document.getElementsByName("username");
    var PassWordElement = document.getElementsByName("password");

    var message = "Please fill all mandatory fields";

    setBorderColor(UserNameElement);
    setBorderColor(PassWordElement);
    if (UserNameElement == null || UserNameElement.value == "") {
        document.getElementById("valueUsername").innerHTML = "Enter username again.";
        UserNameElement.style.borderColor = "red";
    }
    if (PassWordElement == null || PassWordElement.value == "") {
        document.getElementById("valuePassword").innerHTML = "Enter password again.";
        PassWordElement.style.borderColor = "red";
    }
}

function setBorderColor(element) {
    if (element.value == "") {
        element.style.borderColor = "red";
    } else {
        element.style.borderColor = "green";
    }
}
        // Validator({
        //     form: '#form-1',
        //     errorSelector: '.form-check',
        //     rules: [
        //         Validator.isRequired('#username'),
        //         Validator.maxLength('#username', 50),
        //         Validator.isRequired('#password'),
        //         Validator.maxLength('#password', 50),
        //     ]
        // });
