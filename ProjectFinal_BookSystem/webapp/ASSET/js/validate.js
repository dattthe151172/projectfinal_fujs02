
function checkFormValid(){
    var isFormValid = true;

     /* =================================
    Required cua Title Book su dung JQuery
    ===================================*/

    var title = $('#inputTitle');
    var titleMsg = $('#titleMsg');
    if(title.val() == '' ){
        title.removeClass('input-valid');
        title.addClass('input-invalid');
        titleMsg.html('Title book should not be blank').show();
        isFormValid = false;
    }
    else{
        title.removeClass('input-invalid');
        title.addClass('input-valid');
        titleMsg.html('').hide();
    }

    /* =================================
    Required cua Author su dung JQuery
    ===================================*/

    var author = $('#inputAuthor');
    var authorMsg = $('#authorMsg');

    if(author.val() == ''){
        author.removeClass('input-valid');
        author.addClass('input-invalid');
        authorMsg.html('Author should not be blank').show();
        isFormValid = false;
    }else{
        author.removeClass('input-invalid');
        author.addClass('input-valid');
        authorMsg.html('').hide();
    }
    
        /* =================================
    Required cua Category su dung JQuery
    ===================================*/

    var category = $('#inputCategory');
    var categoryMsg = $('#categoryMsg');

    if(category.val() == ''){
        category.removeClass('input-valid');
        category.addClass('input-invalid');
        categoryMsg.html('Category should not be blank').show();
        isFormValid = false;
    }else{
        category.removeClass('input-invalid');
        category.addClass('input-valid');
        categoryMsg.html('').hide();
    }
    
        /* =================================
    Required cua Publisher su dung JQuery
    ===================================*/

    var publisher = $('#inputPublisher');
    var publisherMsg = $('#publisherMsg');

    if(publisher.val() == ''){
        publisher.removeClass('input-valid');
        publisher.addClass('input-invalid');
        publisherMsg.html('Publisher should not be blank').show();
        isFormValid = false;
    }else{
        publisher.removeClass('input-invalid');
        publisher.addClass('input-valid');
        publisherMsg.html('').hide();
    }
    
          /* =================================
    Required cua Image su dung JQuery
    ===================================*/

    var image = $('#inputImage');
    var imageMsg = $('#imageMsg');

    if(image.val() == ''){
        image.removeClass('input-valid');
        image.addClass('input-invalid');
        imageMsg.html('Image should not be blank').show();
        isFormValid = false;
    }else{
        image.removeClass('input-invalid');
        image.addClass('input-valid');
        imageMsg.html('').hide();
    }
    
        /* =================================
    Required cua Title Book su dung JQuery
    ===================================*/

    var title = $('#inputTitle');
    var titleMsg = $('#titleMsg');

    if(title.val() == ''){
        title.removeClass('input-valid');
        title.addClass('input-invalid');
        titleMsg.html('Title book should not be blank').show();
        isFormValid = false;
    }else{
        title.removeClass('input-invalid');
        title.addClass('input-valid');
        titleMsg.html('').hide();
    }
        /* ================================= 
    Required cua Brief Book su dung JQuery
    ===================================*/

    var brief = $('#inputBrief');
    var briefMsg = $('#briefMsg');

    if(brief.val() == ''){
        brief.removeClass('input-valid');
        brief.addClass('input-invalid');
        briefMsg.html('Brief book should not be blank').show();
        isFormValid = false;
    }else{
        brief.removeClass('input-invalid');
        brief.addClass('input-valid');
        briefMsg.html('').hide();
    }
        /* =================================
    Required cua Content Book su dung JQuery
    ===================================*/

    var content = $('#inputContent');
    var contentMsg = $('#contentMsg');

    if(content.val() == ''){
        content.removeClass('input-valid');
        content.addClass('input-invalid');
        contentMsg.html('Content book should not be blank').show();
        isFormValid = false;
    }else{
        content.removeClass('input-invalid');
        content.addClass('input-valid');
        contentMsg.html('').hide();
    }

    return isFormValid;
}

