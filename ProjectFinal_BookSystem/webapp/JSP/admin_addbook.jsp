<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Add Book</title>
<!-- bootstrap link  -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">

<!-- bootstrap link logo  -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<!-- CSS only -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
	crossorigin="anonymous">
<!---->
<link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
<!-- font awesome cdn link  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>
<style>
.container h2 {
	text-align: center;
	font-weight: bold;
	color: #106d3d;
	padding-bottom: 30px;
	margin-top: -30px;
}

.container form label {
	font-weight: bold;
}

.container form textarea {
	overflow: hidden;
	resize: none;
}

.container form button {
	margin-left: 500px;
}

.message {
	color: red;
	font-style: italic;
	font-size: 1rem;
	margin-left: 10px;
}

/* Set the style for the a tag */
.pagination a {
	color: black;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
}

.pagination {
	text-align: center;
	margin-bottom: 2rem;
	position: relative;
	font-size: 1.2rem;
}

/* set the style for the active class */
.pagination a.active {
	background-color: dodgerblue;
	color: white;
}
/* add background color when user hovers on inactive class */
.pagination a:hover:not(.active) {
	background-color: #ddd;
}
</style>
<body>
	<!-- =====  HEADER START  ===== -->
	<jsp:include page="admin_header.jsp"></jsp:include>
	<!-- =====  HEADER END  ===== -->

	<div class="container-fluid">
		<div class="row flex-nowrap">
			<div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-light">
				<div
					class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-dark min-vh-100">
					<div class="m-4"></div>
					<ul
						class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start fs-5" id="menu">
						<li><a href="#" class="nav-link px-0 align-middle text-dark">
								<i class="fs-4 bi-table"></i> <span
								class="ms-1 d-none d-sm-inline text-dark">View List Book</span>
						</a></li>
						<li><a href="#" class="nav-link px-0 align-middle text-dark">
								<i class="fs-4 bi-patch-plus"></i> <span
								class="ms-1 d-none d-sm-inline text-dark">Add Book</span>
						</a></li>
					</ul>
					<hr>
				</div>
			</div>
			<div class="col-9">
				<div class="container">
					<div class="mx-0 p-5 fs-5 m-3" style="width: 115%;">
						<div class="row m-3">
							<h2>
								<span><i class="fs-4 bi-patch-plus"></i> Create Book</span>
							</h2>
						</div>
						<form action="create" method="post" enctype="multipart/form-data"
							onsubmit="return checkFormValid();"
							class="row g-3 needs-validation" id="form" novalidate>
							<div class="col-md-6">
								<label for="validationCustom01" class="form-label">Title
								</label> <input type="text" name="title" class="form-control" id="inputTitle">
								<div class="message" id="titleMsg"></div>
							</div>
							<div class="col-md-6">
								<label for="validationCustom02" class="form-label">Author
								</label> <input type="text" name="author" class="form-control"
									id="inputAuthor">
								<div class="message" id="authorMsg"></div>
							</div>
							<div class="col-md-6">
								<label for="validationCustom02" class="form-label">Category</label>
							   <select name="category" class="form-select" aria-label="Default select example" id="inputCategory">
							        <option value="" selected>Choose</option>
									<option value="Poem">Poem</option>
                                    <option value="Novel">Novel</option>
								</select>
								<div class="message" id="categoryMsg"></div>
							</div>
							<div class="col-md-6">
								<label for="validationCustom02" class="form-label">Publisher
								</label> <input type="text" name="publisher" class="form-control"
									id="inputPublisher">
								<div class="message" id="publisherMsg"></div>
							</div>
							<div class="col-md-12">
								<label for="validationCustom02" class="form-label">Image</label>
								<input type="text" name="image" class="form-control" id="inputImage">
								<div class="message" id="imageMsg"></div>
							</div>
							<div class="col-md-12">
								<label for="validationCustom03" class="form-label">Brief</label>
								<textarea type="text" name="brief" class="form-control"
									id="inputBrief" required rows="3"></textarea>
								<div class="message" id="briefMsg"></div>
							</div>
							<div class="col-md-12">
								<label for="validationCustom03" class="form-label">Content</label>
								<textarea type="text" name="content" class="form-control"
									id="inputContent" required rows="5"></textarea>
								<div class="message" id="contentMsg"></div>
							</div>

							<div class="col-12">
								<button type="submit" class="btn btn-success  btn-md">Create</button>
							</div>
						</form>
						<div class="card-body m-1 col-12"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- custom js file link  -->

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<script src="./ASSET/js/validate.js"></script>
</body>
</html>