<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit List Book</title>
    <!-- bootstrap link  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- bootstrap link logo  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <!---->
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>
<style>
.message {
	color: red;
	font-style: italic;
	font-size: 1rem;
	margin-left: 10px;
}
</style>

<body>
    <!-- =====  HEADER START  ===== -->
	<jsp:include page="admin_header.jsp"></jsp:include>
	<!-- =====  HEADER END  ===== -->
	
    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-light">
                <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-dark min-vh-100">
                    <div class="m-4"></div>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start fs-5"
                        id="menu">
                        <li>
                            <a href="admin_listbook.jsp" class="nav-link px-0 align-middle text-dark">
                                <i class="fs-4 bi-table"></i> <span class="ms-1 d-none d-sm-inline text-dark">View List
                                    Book</span></a>
                        </li>
                        <li>
                            <a href="admin_addbook.jsp" class="nav-link px-0 align-middle text-dark">
                                <i class="fs-4 bi-patch-plus"></i> <span class="ms-1 d-none d-sm-inline text-dark">Add
                                    Book</span>
                            </a>
                        </li>
                    </ul>
                    <hr>
                </div>
            </div>
            <div class="p-4">
                <div class="container-fluid">
                    <div class="mb-3 mx-5" style="max-width: 90%;">
                        <div style="background-color: white;">
                            <h1 class="mt-4 m-lg-4">Edit List Book</h1>
                        </div>
                        <div class="card-body">
                            <form action="EditAdminBook" method="POST" onsubmit="return checkFormValid();" novalidate>
                            <input value ="${book.bookId}"type="hidden" name="id" >
                                <div class="form-group row p-3">
                                    <label class="col-sm-2 col-form-label font-weight-bold d-flex">Title
                                    </label>
                                    <div class="col-sm-4">
                                        <input value ="${book.bookTitle}" type="text" class="form-control px-3" name="name" id="inputTitle">
                                        <div class="message" id="titleMsg"></div>
                                    </div>
                                </div>
                                <div class="form-group row p-3">
                                    <label class="col-sm-2 col-form-label font-weight-bold d-flex">Author
                                    </label>
                                    <div class="col-sm-4">
                                        <input value = "${book.author}"type="text" class="form-control px-3" name="author" id="inputAuthor">
                                        <div class="message" id="authorMsg"></div>
                                    </div>
                                </div>
                                <div class="form-group row p-3">
                                    <label class="col-sm-2 col-form-label font-weight-bold d-flex">Category</label>
                                    <div class="col-sm-4">
                                        <select class="custom-select mr-sm-4 px-5" name="category">
                                            <option value="Poem">Poem</option>
                                            <option value="Novel">Novel</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row p-3">
                                    <label class="col-sm-2 col-form-label font-weight-bold d-flex">Brief
                                    </label>
                                    <div class="col-sm-4">
                                        <input value="${book.brief}"type="text" class="form-control px-3" name="Brief" id="inputBrief">
                                        <div class="message" id="briefMsg"></div>
                                    </div>
                                </div>
                                <div class="form-group row p-3">
                                    <label class="col-sm-2 col-form-label font-weight-bold d-flex">Publisher
                                    </label>
                                    <div class="col-sm-4">
                                        <input value="${book.publisher}"type="text" class="form-control px-3" name="Publisher" id="inputPublisher">
                                        <div class="message" id="publisherMsg"></div>
                                    </div>
                                </div>
                                <div class="form-group row p-3">
                                    <label class="col-sm-2 col-form-label font-weight-bold d-flex">Content
                                    </label>
                                    <div class="col-sm-4">
                                        <input value="${book.content}"type="text" class="form-control px-3" name="Content" id="inputContent">
                                        <div class="message" id="contentMsg"></div>
                                    </div>
                                </div>

                                <div class="form-row align-items-center p-3">
                                    <button type="button" class="btn btn-success mx-1"> <i class="bi bi-save"></i>
                                        <input type="submit" value="Save"></button>
                 
                                    <button type="button" class="btn btn-danger mx-1"> <i class="bi bi-x"></i>
                                        <a href="admin">Cancel</a></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- custom js file link  -->

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
		  <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
	<script src="./ASSET/js/validate.js"></script>
</body>

</html>