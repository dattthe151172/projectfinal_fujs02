<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>View List Book</title>
<!-- bootstrap link  -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">

<!-- bootstrap link logo  -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
<!---->
<link rel="stylesheet"
	href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
<!-- font awesome cdn link  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>
<style>
table {
  counter-reset: section;
}

.count:before {
  counter-increment: section;
  content: counter(section);
}
/* Set the style for the a tag */
.pagination a {
	color: black;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
}

.pagination {
	text-align: center;
	margin-bottom: 2rem;
	position: relative;
	font-size: 1.2rem;
}

/* set the style for the active class */
.pagination a.active {
	background-color: dodgerblue;
	color: white;
}
/* add background color when user hovers on inactive class */
.pagination a:hover:not(.active) {
	background-color: #ddd;
}
</style>
<body>
	<!-- =====  HEADER START  ===== -->
	<jsp:include page="admin_header.jsp"></jsp:include>
	<!-- =====  HEADER END  ===== -->

	<div class="container-fluid">
		<div class="row flex-nowrap">
			<div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-light">
				<div
					class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-dark min-vh-100">
					<div class="m-4"></div>
					<ul
						class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start fs-5"
						id="menu">
						<li><a href="#" class="nav-link px-0 align-middle text-dark">
								<i class="fs-4 bi-table"></i> <span
								class="ms-1 d-none d-sm-inline text-dark">View List Book</span>
						</a></li>
						<li><a href="create" class="nav-link px-0 align-middle text-dark">
								<i class="fs-4 bi-patch-plus"></i> <span
								class="ms-1 d-none d-sm-inline text-dark">Add Book</span>
						</a></li>
					</ul>
					<hr>
				</div>
			</div>
			<div class="col-9">
				<div class="container">
					<div class="mx-0 p-5 fs-5 m-3" style="width: 115%;">
						<form action="" method="POST">
							<div class="row m-3">
								<h2>
									<span>List Book</span>
								</h2>
							</div>
						</form>
						<div class="card-body m-1 col-12">
							<table class="table table">
								<thead class="thead-light table-success">
									<tr>
										<th scope="col" style="width: 1%">STT</th>
										<th scope="col" style="width: 10%">Name</th>
										<th scope="col" style="width: 10%">Author</th>
										<th scope="col" style="width: 8%">Category</th>
										<th scope="col" style="width: 13%">Brief</th>
										<th scope="col" style="width: 13%">Publisher</th>
										<th scope="col" style="width: 16%">Action</th>
									</tr>
								</thead>
								<c:forEach items="${listAllBook}" var="book">
									<tbody>
										<tr>
											<td class="count"></td>										
											<td>${book.bookTitle}</td>
											<td>${book.author}</td>
											<td>${book.category}</td>
											<td>${book.brief}</td>
											<td>${book.publisher}</td>
											<td>
												<div class="d-inline p-0 bg-success rounded-3 fs-6">
													<a href="EditAdminBook?id=${book.bookId}"
														class="text-decoration-none text-dark card-body"> <i
														class="bi bi-pencil-square "></i> Edit
													</a>
												</div>
												<div class="d-inline p-0 bg-danger rounded-3 fs-6">
													<a href="DeleteAdminBook?id=${book.bookId }"
														class="text-decoration-none text-dark card-body"> <i
														class="bi bi-trash"></i> Delete
													</a>
												</div>
											</td>
										</tr>
									</tbody>
								</c:forEach>
							</table>
						</div>

						<c:if test="${endP != 1 }">
							<div class="">
								<div class="pagination">
									<c:if test="${index > 1 }">
										<a href="admin?index=${index-1}">Previous</a>
									</c:if>

									<c:forEach begin="1" end="${endP}" var="i">
										<c:if test="${index == i }">
											<a class="active" href="admin?index=${i}">${i}</a>
										</c:if>
										<c:if test="${index != i }">
											<a href="admin?index=${i}">${i}</a>
										</c:if>
									</c:forEach>
									<c:if test="${index != endP }">
										<a href="admin?index=${index+1}">Next</a>
									</c:if>

								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>