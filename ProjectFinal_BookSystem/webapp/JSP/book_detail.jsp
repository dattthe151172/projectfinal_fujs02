<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Book Detail</title>

<link rel="stylesheet"
	href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

<!-- font awesome cdn link  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

<!-- custom css file link  -->
<link rel="stylesheet" href="./ASSET/css/styles.css">

</head>
<body>

	<!-- =====  HEADER START  ===== -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- =====  HEADER END  ===== -->

	<!-- bottom navbar  -->

	<nav class="bottom-navbar">
		<a href="#home" class="fas fa-home"></a> <a href="#featured"
			class="fas fa-list"></a> <a href="#arrivals" class="fas fa-tags"></a>
		<a href="#reviews" class="fas fa-comments"></a> <a href="#blogs"
			class="fas fa-blog"></a>
	</nav>

	<!-- book detail starts  -->

	<section class="book-detail">

		<!-- =====  BANNER STRAT  ===== -->
		<div class="breadcrumb">
			<ul>
				<li><i class="fas fa-home"></i><a href="Home"> Home</a></li>
				<li>Review book</li>
				<li class="active">${book.bookTitle }</li>
			</ul>
		</div>
		<!-- =====  BREADCRUMB END===== -->


		<div class="swiper book-detail-slider">
		
				<div class="book-detail-slide box">
					<div class="image">
						<img src="${book.image}" alt="">
					</div>
					<div class="content">
						<h3>${book.bookTitle }</h3>
						<li><i class="fas fa-user"> </i> <label>Author:</label> <span>
								${book.author }</span></li>
						<li><i class="fas fa-address-book"></i> <label>Publisher:</label>
							<span> ${book.publisher } </span></li>
						<li></li>
						<li><i class="fas fa-book"> </i> <label>Category:</label> <span>
								${book.category } </span></li>
						<li></li>
						<li><i class="far fa-sticky-note"></i> <label>Brief</label>
							<p>${book.brief }</p></li>
						<li></li>

						<div class="action">
						<a href="AddBookCaseServlet?bookId=${book.bookId}">
							<button type="button"  class="btn btn-success">
								<i class="fas fa-heart"></i> follow
							</button>
							</a>
							<a href="readBook?bookId=${book.bookId}">
							<button type="button" class="btn btn-success">
								<i class="fab fa-readme"></i> read
							</button>
							</a>
						</div>
						<div></div>
					</div>

				</div>
			
			<div class="slide-banner">
				<img src="./ASSET/images/book-banner.jpg" alt="">
			</div>

		</div>
	</section>

	<!-- book detail ends -->


	<!-- new books starts  -->

	<section class="arrivals" id="arrivals">

		<h1>
			<i class="fas fa-cloud-download-alt"></i> new books
		</h1>

		<div class="swiper arrivals-slider">

			<div class="swiper-wrapper">
				<c:forEach items="${listDateDESC}" var="listDateDESC">
					<a href="BookDetail?bookId=${listDateDESC.bookId}" class="swiper-slide box">
						<div class="image">
							<img src="${listDateDESC.image}" alt="">
						</div>
						<div class="content">
							<h3>${listDateDESC.bookTitle}</h3>
							<div class="stars">
								<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
									class="fas fa-star"></i> <i class="fas fa-star"></i> <i
									class="fas fa-star-half-alt"></i>
							</div>
							<div class="read">
								<span>read</span>
							</div>
						</div>
						
					</a>
				</c:forEach>


			</div>

		</div>



	</section>

	<!-- new books end-->


	<!-- =====  FOOTER START  ===== -->
	<jsp:include page="footer.jsp"></jsp:include>
	<!-- =====  FOOTER END  ===== -->


	<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

	<!-- custom js file link  -->
	<script src="./ASSET/js/script.js"></script>

</body>
</html>