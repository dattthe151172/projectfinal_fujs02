<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

</head>
<body>

<!-- footer section starts  -->

		<section class="footer">

			<div class="box-container">

				<div class="box">
					<h3>our locations</h3>
					<a href="#"> <i class="fas fa-map-marker-alt"></i> india
					</a> <a href="#"> <i class="fas fa-map-marker-alt"></i> USA
					</a> <a href="#"> <i class="fas fa-map-marker-alt"></i> russia
					</a> <a href="#"> <i class="fas fa-map-marker-alt"></i> france
					</a> <a href="#"> <i class="fas fa-map-marker-alt"></i> japan
					</a> <a href="#"> <i class="fas fa-map-marker-alt"></i> africa
					</a>
				</div>

				<div class="box">
					<h3>quick links</h3>
					<a href="Home"> <i class="fas fa-arrow-right"></i> home
					</a> <a href="Home#featured"> <i class="fas fa-arrow-right"></i> featured
					</a> <a href="Home#arrivals"> <i class="fas fa-arrow-right"></i> arrivals
					</a> <a href="Home#reviews"> <i class="fas fa-arrow-right"></i> reviews
					</a> <a href="Home#blogs"> <i class="fas fa-arrow-right"></i> blogs
					</a>
				</div>

				<div class="box">
					<h3>extra links</h3>
					<a href="#"> <i class="fas fa-arrow-right"></i> account info
					</a> <a href="#"> <i class="fas fa-arrow-right"></i> ordered items
					</a> <a href="#"> <i class="fas fa-arrow-right"></i> privacy policy
					</a> <a href="#"> <i class="fas fa-arrow-right"></i> payment method
					</a> <a href="#"> <i class="fas fa-arrow-right"></i> our serivces
					</a>
				</div>


			</div>

			<div class="share">
				<a href="#" class="fab fa-facebook-f"></a> <a href="https://www.facebook.com/tiendatt2/"
					class="fab fa-twitter"></a> <a href="https://www.instagram.com/tiendatt0901/" class="fab fa-instagram"></a>
				<a href="#" class="fab fa-linkedin"></a> <a href="https://www.instagram.com/tiendatt0901/"
					class="fab fa-pinterest"></a>
			</div>

			<div class="credit">
				created by <span>Team5_FUJS02</span> | all rights reserved!
			</div>

		</section>



		<!-- footer section ends -->
</body>
</html>