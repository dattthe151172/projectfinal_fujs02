<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


	<!-- header section starts  -->

	<header class="header">

		<div class="header-1">

			<a href="Home" class="logo"> <i class="fas fa-book"></i> bookly
			</a>

			<form action="search" method="get" class="search-form">
				<input type="search" name="search" placeholder="search here..."
					id="search-box"> <label for="search-box"
					class="fas fa-search"></label>
			</form>

			<div class="icons">
				<div id="search-btn" class="fas fa-search"></div>
				<a href="bookcase?id=${sessionScope.id}" class="fas fa-heart"></a>
				<c:if test="${sessionScope.user==null}">
				<a href="login"  class="fas fa-user"> </a>
				</c:if>
				<c:if test="${sessionScope.user!=null}">
				<a href="logout"  class="fas fa-sign-out-alt"> </a>
				</c:if>
				
			</div>

		</div>

		<div class="header-2">
			<nav class="navbar">
				<a href="Home">home</a> <a href="ListBook">list books</a> <a
					href="Home#featured">featured</a> <a href="Home#arrivals">arrivals</a>
				<a href="Home#reviews">reviews</a> <a href="Home#blogs">blogs</a>
			</nav>
		</div>


	</header>

	<!-- header section ends -->
</body>
</html>
