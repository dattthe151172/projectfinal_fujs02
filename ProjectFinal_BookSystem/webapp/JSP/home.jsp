<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Online Book Store Website</title>

<link rel="stylesheet"
	href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

<!-- font awesome cdn link  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

<!-- custom css file link  -->
<link rel="stylesheet" href="./ASSET/css/styles.css">

</head>
<body>

	<!-- header section starts  -->

	<jsp:include page="header.jsp"></jsp:include>

	<!-- header section ends -->

	<!-- bottom navbar  -->

	<nav class="bottom-navbar">
		<a href="#home" class="fas fa-home"></a> <a href="#featured"
			class="fas fa-list"></a> <a href="#arrivals" class="fas fa-tags"></a>
		<a href="#reviews" class="fas fa-comments"></a> <a href="#blogs"
			class="fas fa-blog"></a>
	</nav>


	<div id="close-login-btn"></div>


	<!-- home section starts  -->

	<section class="home" id="home">

		<div class="row">

			<div class="content">
				<h3>upto 75% off</h3>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
					Magnam deserunt nostrum accusamus. Nam alias sit necessitatibus,
					aliquid ex minima at!</p>
				<a href="ListBook" class="btn">view list books</a>
			</div>

			<div class="swiper books-slider">
				<div class="swiper-wrapper">
					<c:forEach items="${listRateDESC}" var="listRateDESC">
						<a href="BookDetail?bookId=${listRateDESC.bookId}"
							class="swiper-slide"> <img src="${listRateDESC.image}" alt=""></a>
					</c:forEach>
				</div>
				<img src="./ASSET/images/stand.png" class="stand" alt="">
			</div>

		</div>

	</section>

	<!-- home section ense  -->

	<!-- icons section starts  -->

	<section class="icons-container">

		<div class="icons">
			<i class="fas fa-shipping-fast"></i>
			<div class="content">
				<h3>free shipping</h3>
				<p>order over $100</p>
			</div>
		</div>

		<div class="icons">
			<i class="fas fa-lock"></i>
			<div class="content">
				<h3>secure payment</h3>
				<p>100 secure payment</p>
			</div>
		</div>

		<div class="icons">
			<i class="fas fa-redo-alt"></i>
			<div class="content">
				<h3>easy returns</h3>
				<p>10 days returns</p>
			</div>
		</div>

		<div class="icons">
			<i class="fas fa-headset"></i>
			<div class="content">
				<h3>24/7 support</h3>
				<p>call us anytime</p>
			</div>
		</div>

	</section>

	<!-- icons section ends -->

	<!-- featured section starts  -->

	<section class="featured" id="featured">

		<h1 class="heading">
			<span>featured books</span>
		</h1>

		<div class="swiper featured-slider">

			<div class="swiper-wrapper">
				<c:forEach items="${listBook }" var="listBook">

					<div class="swiper-slide box">
						<div class="icons">
							<a href="search?search=${listBook.author}" class="fas fa-search"></a>
							 <a href="readBook?bookId=${listBook.bookId}"
								class="fas fa-book-reader"></a> 
								<a href="BookDetail?bookId=${listBook.bookId}"
								class="fas fa-eye"></a>
						</div>
						<div class="image">
							<img src="${listBook.image }" alt="">
						</div>
						<div class="content">
							<h3>${listBook.bookTitle }</h3>
							<a href="AddBookCaseServlet?bookId=${listBook.bookId}" class="btn">Follow</a>
						</div>
					</div>
				</c:forEach>


			</div>

			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>

		</div>

	</section>

	<!-- featured section ends -->

	<!-- newsletter section starts -->

	<section class="newsletter">

		<form action="">
			<h3>subscribe for latest updates</h3>
			<input type="email" name="" placeholder="enter your email" id=""
				class="box"> <input type="submit" value="subscribe"
				class="btn">
		</form>

	</section>

	<!-- newsletter section ends -->

	<!-- arrivals section starts  -->

	<section class="arrivals" id="arrivals">

		<h1 class="heading">
			<span>new books</span>
		</h1>

		<div class="swiper arrivals-slider">
			<div class="swiper-wrapper">
				<c:forEach items="${listDateDESC}" var="listDateDESC">
					<a href="BookDetail?bookId=${listDateDESC.bookId}"
						class="swiper-slide box">
						<div class="image">
							<img src="${listDateDESC.image}" alt="">
						</div>
						<div class="content">
							<h3>${listDateDESC.bookTitle}</h3>
							<div class="stars">
								<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
									class="fas fa-star"></i> <i class="fas fa-star"></i> <i
									class="fas fa-star-half-alt"></i>
							</div>
							<div class="read">
								<span>read</span>
							</div>

						</div>
					</a>
				</c:forEach>

			</div>
		</div>

	</section>
	<!-- arrivals section ends -->

	<!-- deal section starts  -->


	<section class="deal">

		<div class="content">
			<h3>deal of the day</h3>
			<h1>upto 50% off</h1>
			<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Unde
				perspiciatis in atque dolore tempora quaerat at fuga dolorum natus
				velit.</p>
			<a href="ListBook" class="btn">list book now</a>
		</div>

		<div class="image">
			<img src="./ASSET/images/deal-img.jpg" alt="">
		</div>

	</section>

	<!-- deal section ends -->

	<!-- reviews section starts  -->

	<section class="reviews" id="reviews">

		<h1 class="heading">
			<span>client's reviews</span>
		</h1>

		<div class="swiper reviews-slider">

			<div class="swiper-wrapper">

				<div class="swiper-slide box">
					<img src="./ASSET/images/pic-1.png" alt="">
					<h3>john deo</h3>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
						Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique
						facere hic.</p>
					<div class="stars">
						<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star-half-alt"></i>
					</div>
				</div>

				<div class="swiper-slide box">
					<img src="./ASSET/images/pic-2.png" alt="">
					<h3>john deo</h3>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
						Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique
						facere hic.</p>
					<div class="stars">
						<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star-half-alt"></i>
					</div>
				</div>

				<div class="swiper-slide box">
					<img src="./ASSET/images/pic-3.png" alt="">
					<h3>john deo</h3>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
						Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique
						facere hic.</p>
					<div class="stars">
						<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star-half-alt"></i>
					</div>
				</div>
				<div class="swiper-slide box">
					<img src="./ASSET/images/pic-4.png" alt="">
					<h3>john deo</h3>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
						Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique
						facere hic.</p>
					<div class="stars">
						<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star-half-alt"></i>
					</div>
				</div>

				<div class="swiper-slide box">
					<img src="./ASSET/images/pic-5.png" alt="">
					<h3>john deo</h3>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
						Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique
						facere hic.</p>
					<div class="stars">
						<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star-half-alt"></i>
					</div>
				</div>

				<div class="swiper-slide box">
					<img src="./ASSET/images/pic-6.png" alt="">
					<h3>john deo</h3>
					<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
						Aspernatur nihil ipsa placeat. Aperiam at sint, eos ex similique
						facere hic.</p>
					<div class="stars">
						<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star"></i> <i class="fas fa-star"></i> <i
							class="fas fa-star-half-alt"></i>
					</div>
				</div>

			</div>

		</div>

	</section>

	<!-- reviews section ends -->

	<!-- blogs section starts  -->

	<section class="blogs" id="blogs">

		<h1 class="heading">
			<span>our blogs</span>
		</h1>

		<div class="swiper blogs-slider">

			<div class="swiper-wrapper">

				<div class="swiper-slide box">
					<div class="image">
						<img src="./ASSET/images/blog-1.jpg" alt="">
					</div>
					<div class="content">
						<h3>blog title goes here</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
							Optio, odio.</p>
						<a href="#" class="btn">read more</a>
					</div>
				</div>

				<div class="swiper-slide box">
					<div class="image">
						<img src="./ASSET/images/blog-2.jpg" alt="">
					</div>
					<div class="content">
						<h3>blog title goes here</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
							Optio, odio.</p>
						<a href="#" class="btn">read more</a>
					</div>
				</div>

				<div class="swiper-slide box">
					<div class="image">
						<img src="./ASSET/images/blog-3.jpg" alt="">
					</div>
					<div class="content">
						<h3>blog title goes here</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
							Optio, odio.</p>
						<a href="#" class="btn">read more</a>
					</div>
				</div>

				<div class="swiper-slide box">
					<div class="image">
						<img src="./ASSET/images/blog-4.jpg" alt="">
					</div>
					<div class="content">
						<h3>blog title goes here</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
							Optio, odio.</p>
						<a href="#" class="btn">read more</a>
					</div>
				</div>

				<div class="swiper-slide box">
					<div class="image">
						<img src="./ASSET/images/blog-5.jpg" alt="">
					</div>
					<div class="content">
						<h3>blog title goes here</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
							Optio, odio.</p>
						<a href="#" class="btn">read more</a>
					</div>
				</div>

			</div>

		</div>

	</section>

	<!-- blogs section ends -->

	<!-- footer section starts  -->
    <jsp:include page="footer.jsp"></jsp:include>
	<!-- footer section ends -->
	
<!-- loader-container section starts  
<div class="loader-container">
    <img src="./ASSET/images/loader-img.gif" alt="">
</div>
-->

	<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

	<!-- custom js file link  -->
	<script src="./ASSET/js/script.js"></script>

</body>
</html>
