<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>List Books</title>

<link rel="stylesheet"
	href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

<!-- font awesome cdn link  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

<!-- custom css file link  -->
<link rel="stylesheet" href="./ASSET/css/styles.css">


</head>
<body>

	<!-- =====  HEADER START  ===== -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- =====  HEADER END  ===== -->


	<section class="home2">

		<div class="content">
			<h3>Hand Picked Book to your door.</h3>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
				Excepturi, quod? Reiciendis ut porro iste totam.</p>
		</div>

	</section>

	<!-- bottom navbar  -->

	<nav class="bottom-navbar">
		<a href="#home" class="fas fa-home"></a> <a href="#featured"
			class="fas fa-list"></a> <a href="#arrivals" class="fas fa-tags"></a>
		<a href="#reviews" class="fas fa-comments"></a> <a href="#blogs"
			class="fas fa-blog"></a>
	</nav>

	<section class="products">



		<!-- featured section starts  -->

		<section class="featured" id="featured">

			<h1 class="heading" id="listBook">
				<span>list books</span>
			</h1>

			<div class="swiper featured-slider">

				<div class="">
					<c:forEach items="${listAllBook}" var="listAllBook">

						<div class="swiper-slide box">
							<div class="icons">
								<a href="search?search=${listAllBook.author}"
									class="fas fa-search"></a> <a
									href="readBook?bookId=${listAllBook.bookId}"
									class="fas fa-book-reader"></a> <a
									href="BookDetail?bookId=${listAllBook.bookId}"
									class="fas fa-eye"></a>
							</div>
							<div class="image">
								<img src="${listAllBook.image}" alt="">
							</div>
							<div class="content">
								<h3>${listAllBook.bookTitle}</h3>
								<a href="AddBookCaseServlet?bookId=${listAllBook.bookId}" class="btn">follow</a>
							</div>
						</div>
					</c:forEach>
				</div>

			</div>

		</section>

		<!-- featured section ends -->

		<c:if test="${endP != 1 }">
			<div class="">
				<div class="pagination">
					<c:if test="${index > 1 }">
						<a href="ListBook?index=${index-1}#listBook">Previous</a>
					</c:if>

					<c:forEach begin="1" end="${endP}" var="i">
						<c:if test="${index == i }">
							<a class="active" href="ListBook?index=${i}#listBook">${i}</a>
						</c:if>
						<c:if test="${index != i }">
							<a href="ListBook?index=${i}#listBook">${i}</a>
						</c:if>
					</c:forEach>
					<c:if test="${index != endP }">
						<a href="ListBook?index=${index+1}#listBook">Next</a>
					</c:if>

				</div>
			</div>

		</c:if>

		<!-- =====  FOOTER START  ===== -->
		<jsp:include page="footer.jsp"></jsp:include>
		<!-- =====  FOOTER END  ===== -->

		<!-- custom js file link  -->
		<script src="./ASSET/js/script.js"></script>
</body>
</html>