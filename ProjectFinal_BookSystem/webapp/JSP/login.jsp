<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    

    <!-- font awesome cdn link  -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- custom css file link  -->
    <link rel="stylesheet" href="./ASSET/css/login.css">
    
</head>

<body class="img m-5 pt-4"
    style="background-image: url(https://assets.entrepreneur.com/content/3x2/2000/20150115183825-books-reading.jpeg?auto=webp&quality=95&crop=16:9&width=675);">
    <section class="fa-section">
        <div class="container pt-5">
            <div class="row justify-content-center">
                <div class="text-center mb-5 mt-5">
                    <h3 class="fa-heading text-white">Sign In</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-3">
                    <form action="login" method="post" class="form" id="form-1">
                        <div class="form-group row">
                            <div>
                                <input type="text" class="form-control border border-light rounded-pill px-4 p-2"
                                    id="username" name="username" placeholder="Username" novalidate pattern=".{5,50}" required />
                                <span id="valueUsername" class="form-check"></span>
                            </div>
                            <div>
                                <input type="password" class="form-control border border-light rounded-pill px-4 p-2"
                                    id="password" name="password" class="form-control" placeholder="Password" required />
                                <span id="valuePassword" class="form-check"></span>
                            </div>
                            <div class="m-3">
                                <input type="checkbox" name="" id="remember-me">
                                <label for="remember-me" class="text-white"> Remember me</label>
                            </div>
                            <div>
                                <button type="submit" class="form-control form-control-lg btn btn-success submit px-3 p-3 rounded-pill"
                                    onclick="Validator()">Sign In</button>
                            </div>
                        </div>
                    </form>
                 <div style="color: red"> ${message}</div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="./ASSET/js/login.js"></script>
</body>

</html>
