<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Read Book</title>

<link rel="stylesheet"
	href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

<!-- font awesome cdn link  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

<!-- custom css file link  -->
<link rel="stylesheet" href="./ASSET/css/styles.css">

</head>
<style>
.book-detail .breadcrumb {
	margin-left: -70px;
}

.book-detail-slider .header {
	width: 100%;
	height: 20rem;
	font-size: 2rem;
	text-align: center;
	margin-top: 10px;
}

.book-detail-slider .content-book {
margin-top: 50px;
	margin-bottom: 50px;
	font-family: Palatino Linotype, 'sans-serif';
	font-size: 22px;
	display: flex;
}

.book-detail-slider .content-book .image img {
	width: 400px;
	height: 450px;
}

.book-detail .book-detail-slider .content-book p {
	margin-left: 100px;
	margin-top: 30px;
	padding: 5px;
	word-wrap: break-word;
	width: 50%;
	font-family: Palatino Linotype, 'sans-serif';
	font-size: 22px;
}

.book-detail-slider .header h3 {
	text-transform: uppercase;
	margin-top: 30px;
	color: red;
	margin-top: 30px;
	  font-weight: 800;
  color: transparent;
  font-size:80px;
  background: url("https://phandroid.s3.amazonaws.com/wp-content/uploads/2014/05/rainbow-nebula.jpg") repeat;
  background-position: 40% 50%;
  -webkit-background-clip: text;
  position:relative;
  text-align:center;
  line-height:90px;
  letter-spacing: -8px;
}

.heading{
margin-top: 40px;
color: blue;
}

label,
 span{
  top: 30%;
  text-shadow:     0 1px 0 hsl(174,5%,80%),
                   0 2px 0 hsl(174,5%,75%),
                   0 3px 0 hsl(174,5%,70%),
                   0 4px 0 hsl(174,5%,66%),
                   0 5px 0 hsl(174,5%,64%),
                   0 6px 0 hsl(174,5%,62%),
                   0 7px 0 hsl(174,5%,61%),
                   0 8px 0 hsl(174,5%,60%),
  
                   0 0 5px rgba(0,0,0,.05),
                  0 1px 3px rgba(0,0,0,.2),
                  0 3px 5px rgba(0,0,0,.2),
                 0 5px 10px rgba(0,0,0,.2),
                0 10px 10px rgba(0,0,0,.2),
                0 20px 20px rgba(0,0,0,.3);
}

</style>
<body>

	<!-- =====  HEADER START  ===== -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- =====  HEADER END  ===== -->

	<!-- bottom navbar  -->

	<nav class="bottom-navbar">
		<a href="#home" class="fas fa-home"></a> <a href="#featured"
			class="fas fa-list"></a> <a href="#arrivals" class="fas fa-tags"></a>
		<a href="#reviews" class="fas fa-comments"></a> <a href="#blogs"
			class="fas fa-blog"></a>
	</nav>

	<!-- read book starts  -->

	<section class="book-detail">

		<!-- =====  BANNER STRAT  ===== -->
		<div class="breadcrumb">
			<ul>
				<li><i class="fas fa-home"></i><a href="Home"> Home</a></li>
				<li class="active">${book.bookTitle }</li>
			</ul>
		</div>
		<!-- =====  BREADCRUMB END===== -->


		<div class="swiper book-detail-slider">
		
			<div class="header">
				<h3>${book.bookTitle }</h3>
				<li><i class="fas fa-user"> </i> <label>Author: </label><span>${book.author }</span></li>
				<li><i class="fas fa-address-book"></i> <label>Publisher:</label>
					<span> ${book.publisher }</span></li>
				<li></li>
				<li><i class="far fa-calendar-alt"></i> </i> <label>Date:</label> <span>
						${book.dateOfManuf } </span></li>
				<li></li>
				
						
			
			</div>

			<div class="content-book">
				<p>${book.content }</p>

				<div class="image">
					<img src="${book.image}" alt="">
				</div>
			</div>
		</div>

	</section>
	<!-- read book end -->

	<!-- icons section starts  -->

	<section class="icons-container">

		<div class="icons">
			<i class="fas fa-shipping-fast"></i>
			<div class="content">
				<h3>free shipping</h3>
				<p>order over $100</p>
			</div>
		</div>

		<div class="icons">
			<i class="fas fa-lock"></i>
			<div class="content">
				<h3>secure payment</h3>
				<p>100 secure payment</p>
			</div>
		</div>

		<div class="icons">
			<i class="fas fa-redo-alt"></i>
			<div class="content">
				<h3>easy returns</h3>
				<p>10 days returns</p>
			</div>
		</div>

		<div class="icons">
			<i class="fas fa-headset"></i>
			<div class="content">
				<h3>24/7 support</h3>
				<p>call us anytime</p>
			</div>
		</div>

	</section>

	<!-- icons section ends -->




	<!-- =====  FOOTER START  ===== -->
	<jsp:include page="footer.jsp"></jsp:include>
	<!-- =====  FOOTER END  ===== -->


	<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

	<!-- custom js file link  -->
	<script src="./ASSET/js/script.js"></script>

</body>
</html>