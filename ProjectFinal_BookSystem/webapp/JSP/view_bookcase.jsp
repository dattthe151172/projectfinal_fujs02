<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>View Book Care</title>

<link rel="stylesheet"
	href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

<!-- font awesome cdn link  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

<!-- custom css file link  -->
<link rel="stylesheet" href="./ASSET/css/styles.css">

</head>
<style>
.content button {
	font-size: 1.4rem;
	width: 12rem;
}

.heading span {
	font-size: 1.8rem;
	color: #3B907D;
}
</style>
<body>

	<!-- =====  HEADER START  ===== -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- =====  HEADER END  ===== -->

	<nav class="bottom-navbar">
		<a href="#home" class="fas fa-home"></a> <a href="#featured"
			class="fas fa-list"></a> <a href="#arrivals" class="fas fa-tags"></a>
		<a href="#reviews" class="fas fa-comments"></a> <a href="#blogs"
			class="fas fa-blog"></a>
	</nav>

	<section class="products">



		<!--  view book case start  -->

		<section class="featured" id="featured">

			<div class="heading">
				<span><i class="fas fa-fire"></i> The book is following</span>
			</div>
			<div class="swiper featured-slider">

				<div class="">
					<div class="swiper book-detail-slider">
						<!--Book 1-->
						<c:forEach items="${books}" var="book">
							<div class="book-detail-slide box">
								<div class="icons">
									<a href="search?search=${book.author}" class="fas fa-search text-decoration-none"></a> <a
										href="readBook?bookId=${book.bookId}" class="fas fa-book-reader text-decoration-none"></a>
									<a href="BookDetail?bookId=${book.bookId}"
										class="fas fa-eye text-decoration-none"></a>
								</div>
								<div class="image">
									<img src="${book.image}" alt="">
								</div>
								<div class="content">
									<h3>${book.bookTitle }</h3>
									<li><i class="fas fa-address-book"> </i> <label>Author:</label>
										<span> ${book.author }</span></li>
									<li></li>
									<li><i class="fas fa-book"> </i> <label>Category:</label>
										<span> ${book.category } </span></li>
									<li></li>
									<li><i class="fas fa-address-book"> </i> <label>Publisher:</label>
										<span> ${book.publisher }</span></li>
									<li></li>
									<div class="action">
										<a href="DeleteBookCaseServlet?bookId=${book.bookId}">
											<button type="button" class="btn btn-success">Unfollow</button>
										</a> <a href="readBook?bookId=${book.bookId}">
											<button type="button" class="btn btn-success">read</button>
										</a>
									</div>
									<div></div>
								</div>
							</div>
						</c:forEach>
						<!--Book 9-->

					</div>
				</div>


			</div>

		</section>

		<!-- view book case end -->

		<c:if test="${endP != 1 }">
			<div class="">
				<div class="pagination">
					<c:if test="${index > 1 }">
						<a href="bookcase?index=${index-1}&id=${id}">Previous</a>
					</c:if>

					<c:forEach begin="1" end="${endP}" var="i">
						<c:if test="${index == i }">
							<a class="active" href="bookcase?index=${i}&id=${id}">${i}</a>
						</c:if>
						<c:if test="${index != i }">
							<a href="bookcase?index=${i}&id=${id}">${i}</a>
						</c:if>
					</c:forEach>
					<c:if test="${index != endP }">
						<a href="bookcase?index=${index+1}&id=${id}">Next</a>
					</c:if>

				</div>
			</div>
		</c:if>

		<!-- =====  FOOTER START  ===== -->
		<jsp:include page="footer.jsp"></jsp:include>
		<!-- =====  FOOTER END  ===== -->

		<!-- custom js file link  -->
		<script src="./ASSET/js/script.js"></script>
</body>

</html>
